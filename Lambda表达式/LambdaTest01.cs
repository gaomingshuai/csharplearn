﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lambda表达式
{
    public delegate void HelloLambdaDelegate(string studentName);//通过委托测试lambda

    internal class LambdaTest01
    {
       public void Show()
        {
            /**
             * 通过委托测试lambda
             */
            HelloLambdaDelegate helloLambdaDelegate = new HelloLambdaDelegate((studentName) => { Console.WriteLine("学生名" + studentName); });
            helloLambdaDelegate("张三");

            Action action = () => { Console.WriteLine("无返回值，无参数"); };
            action();

            Action<DateTime> action1 = d => { Console.WriteLine("参数的个数和种类需要和泛型的个数和种类对应起来,一个参数，参数为："); };
            action1(DateTime.Now);

            Action<int, int> action2 = (a, b) => { Console.WriteLine("参数的个数和种类需要和泛型的个数和种类对应起来，两个参数，此处的两个参数之和为"+(a+b)); };
            action2(2, 3);




            Func<string> func =() => { return "只有返回值，没有参数"; };
            Console.WriteLine(func());

            Func<string, string> func1 = (str) => { return "一个返回值，一个参数，参数为：" + str; };
            Console.WriteLine(func1("Jack"));

            Func<string, string> func2 = str => str;
            Console.WriteLine("lambda的简略写法，一个返回值，参数和返回值都是："+func2("Jack"));

            Func<int, int, int> func3 = (a, b) => a + b;
            Console.WriteLine("两个参数，一个返回值，两数之和为："+func3(2,3));
            

            
        }
    }
}
