﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 泛型
{
    internal class OutIn
    {
        public void Show()
        {
            IListOut<People> listOut = new ListOutImpl<People>();
            IListOut<People> listOut1 = new ListOutImpl<Teacher>();//协变
            Console.WriteLine(listOut.GetT());
            Console.WriteLine(listOut1.GetT());

            IListIn<Teacher> listIn = new ListInImpl<Teacher>();
            IListIn<Teacher> listIn1 = new ListInImpl<People>(); //逆变
            listIn.Show(new Teacher());
            listIn1.Show(new Teacher());

            IListOutIn<Teacher, People> listOutIn = new ListOutInImpl<Teacher, People>();
            IListOutIn<Teacher, People> listOutIn1 = new ListOutInImpl<Teacher, Teacher>();//协变
            IListOutIn<Teacher, People> listOutIn2 = new ListOutInImpl<People, People>();//逆变
            IListOutIn<Teacher, People> listOutIn3 = new ListOutInImpl<People, Teacher>();//协变逆变
            Console.WriteLine(listOutIn.GetO(new Teacher()));
            Console.WriteLine(listOutIn1.GetO(new Teacher()));
            Console.WriteLine(listOutIn2.GetO(new Teacher()));
            Console.WriteLine(listOutIn3.GetO(new Teacher()));

        }
    }
    class People { }
    class Teacher : People { }


    /// <summary>
    /// 协变-->协变后，T只能返回结果，不能作为参数
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IListOut<out T>
    {
        T GetT();
    }

    class ListOutImpl<T> : IListOut<T>
    {
        public T GetT()
        {
            return default(T);//default关键字：如果是值类型，就返回0。如果是引用类型，就返回null
        }
    }


    /// <summary>
    /// 逆变-->逆变后，T只能作为参数，不能作为返回值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IListIn<in T>
    {
        void Show(T t);
    }

    class ListInImpl<T> : IListIn<T>
    {
        public void Show(T t)
        {
            Console.WriteLine(t);
        }
    }

    /// <summary>
    /// 协变逆变 -->T为逆变，只能作为参数，O为协变，只能作为返回值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="O"></typeparam>
    interface IListOutIn<in T, out O>
    {
        O GetO(T t);
    }

    class ListOutInImpl<T, O> : IListOutIn<T, O>
    {
        public O GetO(T t)
        {
            Console.WriteLine(t);
            return default(O);
        }
    }

}
