﻿Student student = new Student();
student.Age = 20;
Console.WriteLine(student.Age);
class Student
{
    private int age;//字段：私有，用于存储数据

    public int Age { get => age; set => age = value; }//属性：公有，用于和外界通信
}