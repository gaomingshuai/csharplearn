﻿namespace WindowsFormsApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.复制ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.剪切ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.新建ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.doc文档ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.txt文档ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pptToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(364, 181);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 242);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.复制ToolStripMenuItem1,
            this.剪切ToolStripMenuItem1,
            this.新建ToolStripMenuItem2,
            this.删除ToolStripMenuItem,
            this.刷新ToolStripMenuItem1});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(109, 124);
            // 
            // 复制ToolStripMenuItem1
            // 
            this.复制ToolStripMenuItem1.Name = "复制ToolStripMenuItem1";
            this.复制ToolStripMenuItem1.Size = new System.Drawing.Size(108, 24);
            this.复制ToolStripMenuItem1.Text = "复制";
            // 
            // 剪切ToolStripMenuItem1
            // 
            this.剪切ToolStripMenuItem1.Name = "剪切ToolStripMenuItem1";
            this.剪切ToolStripMenuItem1.Size = new System.Drawing.Size(108, 24);
            this.剪切ToolStripMenuItem1.Text = "剪切";
            // 
            // 新建ToolStripMenuItem2
            // 
            this.新建ToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doc文档ToolStripMenuItem2,
            this.txt文档ToolStripMenuItem,
            this.pptToolStripMenuItem2});
            this.新建ToolStripMenuItem2.Name = "新建ToolStripMenuItem2";
            this.新建ToolStripMenuItem2.Size = new System.Drawing.Size(108, 24);
            this.新建ToolStripMenuItem2.Text = "新建";
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.删除ToolStripMenuItem.Text = "删除";
            // 
            // 刷新ToolStripMenuItem1
            // 
            this.刷新ToolStripMenuItem1.Name = "刷新ToolStripMenuItem1";
            this.刷新ToolStripMenuItem1.Size = new System.Drawing.Size(108, 24);
            this.刷新ToolStripMenuItem1.Text = "刷新";
            // 
            // doc文档ToolStripMenuItem2
            // 
            this.doc文档ToolStripMenuItem2.Name = "doc文档ToolStripMenuItem2";
            this.doc文档ToolStripMenuItem2.Size = new System.Drawing.Size(224, 26);
            this.doc文档ToolStripMenuItem2.Text = "doc文档";
            // 
            // txt文档ToolStripMenuItem
            // 
            this.txt文档ToolStripMenuItem.Name = "txt文档ToolStripMenuItem";
            this.txt文档ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.txt文档ToolStripMenuItem.Text = "txt文档";
            // 
            // pptToolStripMenuItem2
            // 
            this.pptToolStripMenuItem2.Name = "pptToolStripMenuItem2";
            this.pptToolStripMenuItem2.Size = new System.Drawing.Size(224, 26);
            this.pptToolStripMenuItem2.Text = "ppt";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ContextMenuStrip = this.contextMenuStrip3;
            this.Controls.Add(this.panel1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.contextMenuStrip3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem 复制ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 剪切ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 新建ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem doc文档ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem txt文档ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pptToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem1;
    }
}