﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void 显示子窗体ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //创建子窗体，并设计他们的父窗体
            Form1 form1 = new Form1();
            form1.MdiParent = this;
            form1.Show();

            Form2 form2 = new Form2();
            form2.MdiParent = this;
            form2.Show();

            Form3 form3 = new Form3();
            form3.MdiParent = this;
            form3.Show();

            Form4 form4 = new Form4();
            form4.MdiParent = this;
            form4.Show();
        }

        private void 水平排布ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void 纵向排列ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }
    }
}
