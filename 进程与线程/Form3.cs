﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 进程与线程
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        bool b = false;
        Thread thread;
        private void button1_Click(object sender, EventArgs e)
        {

            if(b == false)
            {
                thread= new Thread(Play);
                thread.IsBackground = true;
                thread.Start();
                button1.Text = "停止";
                b = true;
            }
            else
            {
                button1.Text = "开始";
                b = false;
            }
        }
        
        void Play()
        {
            while (b)
            {
                Random random = new Random();
                label1.Text = random.Next(0, 9).ToString();
                Thread.Sleep(20);
                label2.Text = random.Next(0, 9).ToString();
                Thread.Sleep(20);
                label3.Text = random.Next(0, 9).ToString();
                Thread.Sleep(20);
                label4.Text = random.Next(0, 9).ToString();
                
            }
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }
    }
}
