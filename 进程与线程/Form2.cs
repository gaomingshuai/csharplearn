﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 进程与线程
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random random = new Random();
            label1.Text = random.Next(0, 9).ToString();
            label2.Text = random.Next(0, 9).ToString();
            label3.Text = random.Next(0, 9).ToString();
            label4.Text = random.Next(0, 9).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(button1.Text == "开始")
            {
                timer1.Enabled = true;
                button1.Text = "停止";
            }
            else
            {
                timer1.Enabled = false;
                button1.Text = "开始";
            }
        }
    }
}
