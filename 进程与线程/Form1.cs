﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 进程与线程
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Thread thread;
        private void button1_Click(object sender, EventArgs e)//点击按钮输出1——100000000的数字
        {
            thread = new Thread(Test);//为防止主线程占用，出现假死状态，为新事务绑定线程，此处方法名不用加括号

            thread.IsBackground = true;//设置为后台线程，当前台线程关闭后，此线程也关闭

            thread.Start();//标记此线程处于就绪状态，不是执行状态，具体的执行时间由cpu决定。

        }

        void Test()
        {
            for (int i = 0; i < 100000000; i++)
            {
                textBox1.Text = i.ToString();//跨线程访问（.net默认不允许，需要手动不让它检查出来），新线程访问主线程创建的组件。
            }
        }

        private void Form1_Load(object sender, EventArgs e)//窗体的加载事件，此处为了取消跨线程访问检查
        {
            Control.CheckForIllegalCrossThreadCalls = false;//取消跨线程访问检查
        }

        //防止窗体主线程关闭，而新线程还去访问主线程创建的组件，所造成的异常
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)//窗体正在关闭事件
        {
            if (thread != null)
                thread.Abort();//窗体关闭，主线程关闭，新线程还活着，终止它
        }
    }
}
