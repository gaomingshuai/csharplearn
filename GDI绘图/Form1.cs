﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI绘图
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // 点击按钮，绘画一条直线
        private void button1_Click(object sender, EventArgs e)
        {
            PaintLine();
            
        }
        /// <summary>
        /// 绘画一条直线：  一根笔，颜色，一张纸，两个点，绘制直线的对象
        /// </summary>
        void PaintLine()
        {
            // 创建GDI对象
            Graphics graphics = this.CreateGraphics();//new Graphics();

            //创建笔
            Pen redPen = new Pen(Brushes.Red);//创建一个红笔

            //两个点
            Point point01 = new Point(20, 30);
            Point point02 = new Point(150, 100);

            //绘画
            graphics.DrawLine(redPen, point01, point02);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        int i = 0;
        bool showRectangle = false;
        bool showPie = false;
        bool showTxt = false;
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            textBox1.Text = i.ToString();
            i++;
            PaintLine();
            if(showRectangle) PaintRectangle();
            if (showPie) PaintPie();
            if (showTxt) PaintTxt();
        }

        //绘画一个矩形
        private void button2_Click(object sender, EventArgs e)
        {
            PaintRectangle();
            showRectangle = true;
        }

        //绘制一个矩形
        void PaintRectangle()
        {
            // 创建GDI对象
            Graphics graphics = this.CreateGraphics();//new Graphics();

            //创建笔
            Pen blackPen = new Pen(Brushes.Black);//创建一个黑笔

            //创建一个矩形
            Rectangle rectangle = new Rectangle(new Point(20, 20), new Size(100, 60));

            graphics.DrawRectangle(blackPen, rectangle);

        }

        //绘制一个扇形
        void PaintPie()
        {
            // 创建GDI对象
            Graphics graphics = this.CreateGraphics();//new Graphics();

            //创建笔
            Pen bluePen = new Pen(Brushes.Blue);//创建一个红笔

            //创建一个矩形
            Rectangle rectangle = new Rectangle(new Point(20, 20), new Size(100, 60));

            //给定角度并绘制
            graphics.DrawPie(bluePen, rectangle,30,150);

        }

        private void button3_Click(object sender, EventArgs e)//点击绘制扇形
        {
            PaintPie();
            showPie = true;
        }

        private void button4_Click(object sender, EventArgs e)//点击绘制文本
        {
            PaintTxt();
            showTxt = true;
        }
        void PaintTxt()
        {
            // 创建GDI对象
            Graphics graphics = this.CreateGraphics();//new Graphics();
            graphics.DrawString("用GDI绘制的文字", new Font("正楷", 20, FontStyle.Bold), Brushes.Black, new Point(300, 300));
        }
    }
}
