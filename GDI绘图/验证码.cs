﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI绘图
{
    public partial class 验证码 : Form
    {
        public 验证码()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //验证码确切的值
            Random random = new Random();
            string str = null;
            for (int i = 0; i < 6; i++)
            {
                str += random.Next(0, 9);
                
            }
         
            //创建GDI对象
            Bitmap bitmap = new Bitmap(250, 100);//制作的图片
            Graphics graphics = Graphics.FromImage(bitmap);//在制作的图片上绘制

            for (int i = 0; i < 6; i++)//绘制字体，颜色，位置不同的文本
            {
                Point point = new Point(i*40,random.Next(15,30));
                string[] fonts = { "微软雅黑", "宋体", "黑体","隶书","正楷" };
                Color[] colors = { Color.Yellow, Color.Black, Color.Orange, Color.Blue, Color.Red, Color.Aqua };
                graphics.DrawString(str[i].ToString(), new Font(fonts[random.Next(0, fonts.Length)], 50, FontStyle.Bold), new SolidBrush(colors[random.Next(0, colors.Length)]),point);
            }

            for (int i = 0; i < 100; i++)//为让其模糊，添加横线
            {
                Point point01 = new Point(random.Next(0, bitmap.Width), random.Next(0, bitmap.Height));
                Point point02 = new Point(random.Next(0, bitmap.Width), random.Next(0, bitmap.Height));
                graphics.DrawLine(new Pen(Brushes.Green), point01, point02);
            }

            for (int i = 0; i < 800; i++)//为让其模糊添加像素颗粒
            {
                Point point03 = new Point(random.Next(0, bitmap.Width), random.Next(0, bitmap.Height));
                bitmap.SetPixel(point03.X,point03.Y,Color.Gray);
            }

            //将图片镶嵌到PictureBox中
            pictureBox1.Image = bitmap;

        }
    }
}
