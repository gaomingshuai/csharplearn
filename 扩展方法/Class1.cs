﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 扩展方法
{
    internal class Class1
    {

    }
    public sealed class Person
    {
        public string? name { get; set; }
        public string? phone { get; set; }

        public string GetPhone()//要被扩展的方法
        {
            return phone;
        }
    }

    //定义一个静态类，再定义一个静态方法，将本身和要扩展的方法所在的类的对象添加到参数列表
    public static class PersonExtend
    {
        public static void GetPhoneExtend(this Person person)//已经扩展的方法，可执行完原来的操作，还可添加其他操作
        {
            Console.WriteLine(person.GetPhone());
            Console.WriteLine(person.name);
        }
    }



}
