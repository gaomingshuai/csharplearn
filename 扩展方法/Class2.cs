﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 扩展方法
{
    internal class Class2
    {
    }

    public interface IOperation//要扩展的接口
    {
        public int Add(int a, int b);
    }

    public static class IOperationExtend//该接口的扩展静态类
    {
        public static int Subtract(this IOperation operation, int a,int b)
        {
            return a - b;
        }

        public static int Multiplicate(this IOperation operation, int a,int b)
        {
            return a * b;
        }
        
        public static int Divide(this IOperation operation, int a,int b)
        {
            return a / b;
        }
    }


    public class OperationImpl : IOperation //实现类
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
    }

}
