﻿using 扩展方法;

/*Person person = new Person();
person.name = "小李";
person.phone = "1245654323456543";
PersonExtend.GetPhoneExtend(person);*/

OperationImpl operation = new OperationImpl();
//接口本身约定的方法
Console.WriteLine(operation.Add(1,2));

//扩展类扩展的方法
Console.WriteLine(operation.Subtract(1,2));
Console.WriteLine(operation.Multiplicate(1,2));
Console.WriteLine(operation.Divide(1,2));