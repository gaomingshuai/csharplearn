﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 委托
{
    internal class Class4
    {

        public void Show()
        {
            Action action = Method1; //等同于new Action(Method1);
            action =(Action)MulticastDelegate.Combine(action, new Action(Method2));//通过方法，添加委托的指向
            action += new Action(Method3);//通过重载后的赋值运算符，添加委托的指向
            action += () => { Console.WriteLine("我是Lambda"); };//通过重载后的赋值运算符，添加委托的指向
            action();
            Console.WriteLine("========++++++++++++===========");
            action = (Action)MulticastDelegate.Remove(action, new Action(Method3));//通过方法，减少委托的指向
            action -= Method2;//通过重载后的赋值运算符，减少委托的指向
            action();
        }

        public void Method1()
        {
            Console.WriteLine("我是方法Method1");
        } 
        
        public void Method2()
        {
            Console.WriteLine("我是方法Method1");
        }  
        
        public void Method3()
        {
            Console.WriteLine("我是方法Method1");
        }
    }
}
