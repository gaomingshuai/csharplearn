﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 委托
{

    delegate bool StudentIsVip(Student student);
    internal class Class2
    {
        public void Show()
        {
            List<Student> students = new List<Student>();
            students.Add(new Student() { name = "ant 1 号", price = 399 });
            students.Add(new Student() { name = "ant 2 号", price = 499 });
            students.Add(new Student() { name = "ant 3 号", price = 599 });
            students.Add(new Student() { name = "ant 4 号", price = 599 });
            students.Add(new Student() { name = "ant 5 号", price = 699 });

            StudentIsVip studentIsVip = new StudentIsVip(FindVipStudent);
            VipStudent(students, studentIsVip);
        }

        public bool FindVipStudent(Student student)
        {
            if (student.price >= 599) return true;
            else return false;
        }

        public void VipStudent(List<Student> students, StudentIsVip studentIsVip)//将委托作为参数，完成逻辑方面判断
        {
            foreach (Student student in students)
            {
                if (studentIsVip(student))//拿取委托的逻辑判断直接使用。
                {
                    Console.WriteLine(student.name + "是VIP学员");
                }
            }

        }
    }

    class Student
    {
        public string? name;
        public int price;

    }
}
