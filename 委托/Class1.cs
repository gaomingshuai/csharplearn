﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 委托
{
    delegate void HelloDelegate(string str);//定义委托
    internal class Class1
    {
        public void Show()
        {
            HelloDelegate helloDelegate = new HelloDelegate(Hello);//创建委托实例
            helloDelegate("你好，我是委托");//调用委托的简略写法
            helloDelegate.Invoke("你好，我是委托");//调用委托，作用同上

            Hello("你好，我不是委托");
        }

        public void Hello(string str)
        {
            Console.WriteLine(str);
        }
    }

}
