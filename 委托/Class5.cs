﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 委托
{
    delegate void EventDelegate();
    internal class Class5
    {
        public void Show()
        {
            InvokeDifine invokeDifine = new InvokeDifine();
            EventFunc eventFunc = new EventFunc();

            invokeDifine.StudentEvent += eventFunc.Student1;
            invokeDifine.StudentEvent += eventFunc.Student2;

            invokeDifine.ToInvoke();
        }
    }

    class InvokeDifine
    {
        public event EventDelegate StudentEvent;
        public void ToInvoke()
        {
            StudentEvent?.Invoke();
        }
    }

    class EventFunc
    {
        public  void Student1()
        {
            Console.WriteLine("我是1号学员");
        }
        public void Student2()
        {
            Console.WriteLine("我是2号学员");
        }
    }
    
}
