﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 委托
{
    public class Class3
    {
        public void Show()
        {
            Action<string> action = new Action<string>(Method1);//官方中没有返回值的泛型委托
            action("我是没有返回值的泛型委托");

            Func<string,string> func = new Func<string,string>(Method2);
            Console.WriteLine(func("我是有返回值的泛型委托"));
        }

        public void Method1(string str)
        {
            Console.WriteLine(str);
        } public string Method2(string str)
        {
            return str;
        }
    }
}
