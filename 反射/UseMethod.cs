﻿using ForReflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace 反射
{
    internal class UseMethod
    {
        /// <summary>
        /// 利用泛型调用方法之 普通方法、重构方法、静态方法
        /// </summary>
        public void ShowUseMethodByReflect()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.Methods");
            //3、根据类型创建实例
            object? objReflection = Activator.CreateInstance(type);
            //4、打印该类型的所有方法即所有参数（提示作用）
            foreach (var method in type.GetMethods())//方法
            {
                Console.Write(method.Name+": ");
                foreach (var param in method.GetParameters())//参数
                {
                    Console.Write(param.Name+"-"+param.ParameterType);
                }
                Console.WriteLine();
            }
            //5、根据方法名获取方法，并存储到 MethodInfo
            MethodInfo methodInfo1 = type.GetMethod("Test1");//获取普通无参的Test1
            MethodInfo methodInfo2 = type.GetMethod("Test3",new Type[] {typeof(int),typeof(string)});//获取重构方法中两个参数的Test3
            MethodInfo methodInfo3 = type.GetMethod("Test3", new Type[] { typeof(int) });//获取重构方法中int类型参参数的Test3
            MethodInfo methodInfo4 = type.GetMethod("Test3", new Type[] { typeof(string) });//获取重构方法中string类型参参数的Test3
            MethodInfo methodInfo5 = type.GetMethod("Test5");//获取静态方法中string类型参参数的Test5

            //6、传入对象实例，参数列表调用方法
            methodInfo1.Invoke(objReflection, null);
            methodInfo2.Invoke(objReflection, new object[] {134,"string类型"});
            methodInfo3.Invoke(objReflection, new object[] {123});
            methodInfo4.Invoke(objReflection, new object[] {"单参，string类型"});
            methodInfo5.Invoke(objReflection, new object[] {"单参，string类型"});
            methodInfo5.Invoke(null, new object[] {"单参，string类型"});//静态，等同于上一条
        }

        /// <summary>
        /// 反射调用私有方法
        /// </summary>
        public void ShowUsePrivateMethod()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.Methods");
            //3、根据类型创建实例
            object? objReflection = Activator.CreateInstance(type);
            //4、获取指定私有方法
            MethodInfo methodInfo = type.GetMethod("Test4",BindingFlags.NonPublic|BindingFlags.Instance);//私有无参的Test4,两个标志参数分别表示 私有和创建实例
            //5、调用方法
            methodInfo.Invoke(objReflection, null);


            
        }

        /// <summary>
        /// 反射调用泛型方法    
        /// </summary>
        public void ShowUseGenericMethod()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.GenericMethod");
            //3、根据类型创建实例
            object? objReflection = Activator.CreateInstance(type);
            //4、获取指定泛型方法，
            MethodInfo methodInfo = type.GetMethod("Test");//普通类中的泛型方法
            //5、确定方法的参数类型和个数
            var methodGeneric = methodInfo.MakeGenericMethod(new Type[] { typeof(string), typeof(int) });
            //6、调用方法
            methodGeneric.Invoke(objReflection, new object[] {"string类型",124});

        }


        /// <summary>
        /// 反射调用泛型类中的泛型方法    
        /// </summary>
        public void ShowUseMethodInGenericClass()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.GenericClass`2");//一定要加占位符啊！！！
            //3、指定泛型类型约束的类型和个数
            Type? makeType = type.MakeGenericType(new Type[] { typeof(int), typeof(string) });
            //4、根据编辑过的类型创建实例
            object? objReflection = Activator.CreateInstance(makeType);
            //5、获取泛型类中的泛型方法，
            MethodInfo methodInfo = makeType.GetMethod("Test");//泛型类中的泛型方法
            //6、确定方法的参数类型和个数
            var methodGeneric = methodInfo.MakeGenericMethod(new Type[] { typeof(string), typeof(int) });
            //7、调用方法
            methodGeneric.Invoke(objReflection, new object[] { "string类型", 124 });

        }
  
        

        /// <summary>
        /// 反射操纵属性 
        /// </summary>
        public void showUseProperty()
        {

            Student student = new Student();
            student.id = 1;
            student.name = "张三";
            student.studentAddress = "小李村";

            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.Student");//一定要加占位符啊！！！
            //3、根据类型实例化
            object? objReflectionStudent = Activator.CreateInstance(type);

            /**
             * 方式一：循环
             */
            //4、获取属性
            foreach (var prop in type.GetProperties())
            {
                //GetValue获取属性的值时，要传入实例化的对象，让反射知道时那个对象的字段（此处测验通过反射拿到我们创建对象的值）
                Console.WriteLine($"普通创建对象：{prop.PropertyType}+{prop.Name}={prop.GetValue(student)}");


                //为通过放射的方式创建的对象设置属性值，并输出
                if (prop.Name.Equals("id"))
                {
                    prop.SetValue(objReflectionStudent,111);
                }
                if (prop.Name.Equals("name"))
                {
                    prop.SetValue(objReflectionStudent,"李四");
                }
                if (prop.Name.Equals("studentAddress"))
                {
                    prop.SetValue(objReflectionStudent,"大赵庄");
                }
                Console.WriteLine($"反射创建对象：{prop.PropertyType}+{prop.Name}={prop.GetValue(objReflectionStudent)}");
            }
            /**
             * 方式二：API
             */
            PropertyInfo[] propertyInfo = type.GetProperties();//获取所有的属性
            PropertyInfo property = type.GetProperty("id");//获取指定属性

        } 
        
        /// <summary>
        /// 反射操纵字段 
        /// </summary>
        public void showUseField()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.Student");//一定要加占位符啊！！！
            //3、根据类型实例化
            object? objReflectionStudent = Activator.CreateInstance(type);

            /**
             * 方式一：循环
             */
            //4、获取属性
            foreach (var field in type.GetFields())
            {
                //为通过放射的方式创建对象，并对其字段赋值，并输出
                if (field.Name.Equals("age"))
                {
                    field.SetValue(objReflectionStudent,111);
                }

                //GetValue获取字段的值时，要传入实例化的对象，让反射知道时那个对象的字段（此处测验通过反射拿到我们创建对象的值）
                Console.WriteLine($"反射创建对象：{field.FieldType}+{field.Name}={field.GetValue(objReflectionStudent)}");
            }
            /**
             * 方式二：API
             */
            FieldInfo[] fieldInfos = type.GetFields();//获取所有的属性
            FieldInfo fieldInfo = type.GetField("age");//获取指定属性

        }

    }
}
