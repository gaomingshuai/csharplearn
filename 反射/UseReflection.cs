﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ForReflection;

namespace 反射
{
    internal class UseReflection
    {
        /// <summary>
        /// 创建对象，无参版
        /// </summary>
        public void ShowNoParamCreate()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.DBMysql");
            //3、根据类型创建对象
            object? objDB = Activator.CreateInstance(type);
            //4、类型转换
            IDBHelper? dBHelper = objDB as IDBHelper;
            //5、像正常的类一样调用其中的方法
            dBHelper.Query();
        }


        /// <summary>
        /// 创建对象，有参数版
        /// </summary>
        public void ShowHaveParamCreate()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.DBSqlHaveParametrrs");

            //3、查看这个类型下的所有构造方法（用于对他人的代码，并不知道都设置了什么构造函数的时候使用，仅起到提示作用）
            foreach (ConstructorInfo constructor in type.GetConstructors())//获取本类的所有构造方法
            {
                Console.Write(constructor.Name + ": ");

                foreach (var param in constructor.GetParameters())
                {
                    Console.Write(param.ParameterType+"-"+param.Name+"; ");
                }
                Console.WriteLine();
            }
            //4、根据类型创建对象，这里演示分别通过（无参，string类型参数、int类型参数）的构造函数的演示
            object? objDB1 = Activator.CreateInstance(type);//通过无参构造创建对象
            object? objDB2 = Activator.CreateInstance(type,new object[] {"string类型参数"});//通过一个参数，且参数类型为string的构造函数创建
            object? objDB3 = Activator.CreateInstance(type,new object[] {123});//通过一个参数，且参数类型为int的构造函数创建
            object? objDB4 = Activator.CreateInstance(type,new object[] {"string类型参数",124});//通过两个参数，且参数类型为string，int的构造函数创建
            //5、类型转换
            IDBHelper? dBHelper1 = objDB1 as IDBHelper;//无参对象转化
            IDBHelper? dBHelper2 = objDB2 as IDBHelper;//string 类型单参对象转化
            IDBHelper? dBHelper3 = objDB3 as IDBHelper;//int 类型单参对象转化
            IDBHelper? dBHelper4 = objDB4 as IDBHelper;//string，int 类型双参对象转化
            //6、像正常的类一样调用其中的方法
            dBHelper1.Query();
            dBHelper2.Query();
            dBHelper3.Query();
            dBHelper4.Query();
        }


        /// <summary>
        /// 利用反射创建构造方法 私有 的对象
        /// </summary>
        public void ShowPrivateConstructCreate()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.PrivateConstruct");
            //3、根据类型创建对象   ------  此处是通过 私有的 构造方法，创建的方式
            object? objDB = Activator.CreateInstance(type,true);
            //4、类型转换
            IDBHelper? dBHelper = objDB as IDBHelper;
            //5、像正常的类一样调用其中的方法
            dBHelper.Query();
        }


        /// <summary>
        /// 创建泛型类对象,目标泛型类为：    public class GenericClass<T,K>
        /// </summary>
        public void ShowGenericCreate()
        {
            //1、加载dll文件（添加目标引用或将目标dll文件添加到当前目录）
            Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
            //2、获取类型，要完整类型（命名空间名 . 类名）
            Type? type = assembly.GetType("ForReflection.GenericClass`2");// `2 代表占用两位，由后面进行泛型内部约定的补充
            //3、根据获取的类型，制作其泛型带有确切类名的类型
            Type? makeType = type.MakeGenericType(new Type[] { typeof(int), typeof(string) });

            //4、创建泛型类对象
            object? objGeneric = Activator.CreateInstance(makeType);

            //5、类型转换
            GenericClass<int, string>? genericClass = objGeneric as GenericClass<int, string>;

            //6、调用泛型类中的方法
            genericClass.Query();

        }




    }

    
}
