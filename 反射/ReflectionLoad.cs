﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace 反射
{
    internal class ReflectionLoad
    {
        public void Show()
        {


            Console.WriteLine("---------------------reflection----------------------");

            try
            {
                //加载方式一：dll文件的文件名(加引用或当前目录)
                //Assembly assembly = Assembly.Load("ForReflection");
                //加载方式二：dll文件的绝对路径
                // Assembly assembly = Assembly.LoadFile(@"D:\WorkSpace\VisioStudio\ConsoleApp1\ForReflection\bin\Debug\net6.0\ForReflection.dll");

                //加载方式三：dll文件的完全限定名(加引用或当前目录)
                //Assembly assembly = Assembly.LoadFrom("ForReflection.dll");
                //加载方式四：dll文件的绝对路径
                Assembly assembly = Assembly.LoadFrom(@"D:\WorkSpace\VisioStudio\ConsoleApp1\ForReflection\bin\Debug\net6.0\ForReflection.dll");


                foreach (var type in assembly.GetTypes())//获取目标项目的所有类型
                {
                    Console.WriteLine("类名：  " + type.Name);

                    foreach (var method in type.GetMethods()) //获取本类的方法
                    {
                        Console.WriteLine("方法名：" + method.Name);
                    }
                    Console.WriteLine();

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
