﻿using 反射;

/*Console.WriteLine("---------------------------加载反射的四种方式---------------------------");
ReflectionLoad reflectionLoad = new ReflectionLoad();
reflectionLoad.Show();

Console.WriteLine("---------------------------使用反射创建对象---------------------------");
UseReflection useReflection = new UseReflection();
useReflection.ShowNoParamCreate();
Console.WriteLine("+++++++++++++++————————————++++++++++");
useReflection.ShowHaveParamCreate();
Console.WriteLine("+++++++++++++++————————————++++++++++");
useReflection.ShowPrivateConstructCreate();
Console.WriteLine("+++++++++++++++————————————++++++++++");
useReflection.ShowGenericCreate();*/

Console.WriteLine("---------------------------使用反射调用方法---------------------------");
UseMethod useMethod = new UseMethod();
useMethod.ShowUseMethodByReflect();
Console.WriteLine("________________________++++++++++______________________________");
useMethod.ShowUsePrivateMethod();
Console.WriteLine("________________________++++++++++______________________________");
useMethod.ShowUseGenericMethod();
Console.WriteLine("________________________++++++++++______________________________");
useMethod.ShowUseMethodInGenericClass();
Console.WriteLine("________________________++++++++++______________________________");
useMethod.showUseProperty();
Console.WriteLine("________________________++++++++++______________________________");
useMethod.showUseField();