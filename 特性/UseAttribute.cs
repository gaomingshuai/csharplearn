﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 特性.attributes;
using 特性.manager;

namespace 特性
{
    
    public class UseAttribute
    {
        public void ShowMyAttributeTest()
        {
            Student student = new Student();
            student.id = 1001;
            student.name = "张三";
            MyAttributeManager manager = new MyAttributeManager();
            manager.Show(student);
        }
    }


    [My(123,Description ="自定义特性属性",Remark ="自定义特性字段")]//调用了带有int类型参数的构造函数，并给属性，字段赋值，但方法不行
    class Student
    {
        public int id { get; set; }
        public string? name { get; set; }

        public void Study()
        {
            Console.WriteLine($"{id}号学员的姓名为：{name}，他正在学习！！");
        }

        [My]//方法上添加
        [return:My]//返回值添加注解
        public string StudyObject([My] string subject)//参数添加注解 
        {
            Console.WriteLine($"{id}号学员的姓名为：{name}，他正在学习{subject}！！");
            return subject;
        }
    }
}
