﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 特性.attributes
{
    /// <summary>
    /// 自定义特性
    /// </summary>

    ////指定本特性 可添加到什么地方(可多写,用|分割)，是否允许重复使用，是否可被继承
    [AttributeUsage(AttributeTargets.All ,Inherited =true)]
    public class MyAttribute : Attribute
    {

        public string Description { get; set; }//定义属性

        public string Remark = null; //定义字段
        public MyAttribute()//定义无参构造
        {
            Console.WriteLine("MyAttribute特性的无参构造！！");
        }
        public MyAttribute(int id)//定义含参构造 
        {
            Console.WriteLine($"MyAttribute特性的含参构造,参数为{id}");
        }

        public void ShowThisType()//定义方法
        {
            Console.WriteLine($"this is {nameof(MyAttribute)},type is {typeof(MyAttribute)}");
        }
    }
}
