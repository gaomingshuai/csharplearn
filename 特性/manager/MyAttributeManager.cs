﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 特性.attributes;

namespace 特性.manager
{
    internal  class MyAttributeManager
    {
        public void Show(Student student)
        {
            Type type = typeof(Student);//获取类型，等同于student.GetType();
            if (type.IsDefined(typeof(MyAttribute), true))//若类上添加了指定的特性
            {
                MyAttribute attribute = (MyAttribute)type.GetCustomAttributes(typeof(MyAttribute), true)[0];//实例化该特性
                Console.WriteLine($"{attribute.Description}---{attribute.Remark}");//打印特性的属性或字段
                attribute.ShowThisType();//调用特性方法
            }

            student.Study();
            Console.WriteLine(student.StudyObject("数学"));
        }
    }
}
