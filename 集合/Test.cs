﻿namespace 集合
{
    internal class Test
    {
       
        public void ListDemo()
        {
            List<int> list = new List<int>();

            list.Add(0);
            int[] ints = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,10,10};
            list.AddRange(ints);

            Console.WriteLine();
            Console.WriteLine("集合的元素个数"+list.Count);
            Console.WriteLine("在内部数据结构不调整的情况下，初始容量为：" + list.Capacity);
            Console.WriteLine("集合内是否含有元素10"+list.Contains(10));

            Console.WriteLine();

        }
        void Out(List<int> list)
        {
            foreach (var item in list)
            {
                Console.Write(item + " ");
            }
        }
    }
}
