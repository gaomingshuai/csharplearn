﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 网络编程Socket客户端
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Socket socketSender;
        Thread threadRecive;
        private void button1_Click(object sender, EventArgs e)//连接按钮
        {
            //创建负责通信的Socket
           socketSender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //拼接字符串，制作所要连接的ip地址和端口号
            IPAddress iP = IPAddress.Parse(textServer.Text);
            IPEndPoint point = new IPEndPoint(iP, Convert.ToInt32(textPort.Text));

            //建立连接
            socketSender.Connect(point);

            ShowMsg("连接成功");

            //开启一个新线程，不停的接收服务器发来的消息
            threadRecive = new Thread(Recive);
            threadRecive.IsBackground = true;
            threadRecive.Start();
        }

        void ShowMsg(string msg)
        {
            textLog.AppendText(msg + "\r\n");
        }


        /// <summary>
        /// 客户端给服务器端发送信息, 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)//发送按钮
        {
            string message = textMessage.Text.Trim();
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(message);
            socketSender.Send(buffer);
        }

        string flag;
        int i = 1;
        /// <summary>
        /// 客户端持续接收服务器端发来的消息
        /// </summary>
        void Recive()
        {
            while (true)
            {
                
                byte[] buffer = new byte[1024*1024*10];
                int res = socketSender.Receive(buffer);
                if (res == 0 )
                {
                    break;
                }
                string message = System.Text.Encoding.UTF8.GetString(buffer, 0, res);

                if (i == 1) { 
                    flag = message;
                }
                else {
                    ShowMsg(socketSender.RemoteEndPoint + "：" + message);
                }
                i++;
                if (message.Equals("off"))//如果收到服务其发出off关闭请求，断开连接
                {
                    //向服务器发出关闭请求后，关闭自己
                    string str = flag + ",off";
                    byte[] buffer2 = System.Text.Encoding.UTF8.GetBytes(str);
                    socketSender.Send(buffer);
                    socketSender.Close();
                    socketSender = null;
                    break;
                }
            }
            if(socketSender == null)
            {
                this.Close();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (socketSender != null)
            {
                //向服务器发出关闭请求后，关闭自己
                string message =flag+ ",off";
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(message);
                socketSender.Send(buffer);
                socketSender.Close();
            }

            if (threadRecive != null)
                threadRecive.Abort();


        }
    }
}
