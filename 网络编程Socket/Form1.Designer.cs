﻿namespace 网络编程Socket
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textClient = new System.Windows.Forms.TextBox();
            this.textport = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboxSocket = new System.Windows.Forms.ComboBox();
            this.textlog = new System.Windows.Forms.TextBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.textFileName = new System.Windows.Forms.TextBox();
            this.btnChoose = new System.Windows.Forms.Button();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.btnSender = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textClient
            // 
            this.textClient.Location = new System.Drawing.Point(45, 38);
            this.textClient.Multiline = true;
            this.textClient.Name = "textClient";
            this.textClient.Size = new System.Drawing.Size(149, 25);
            this.textClient.TabIndex = 0;
            this.textClient.Text = "192.168.8.104";
            // 
            // textport
            // 
            this.textport.Location = new System.Drawing.Point(200, 39);
            this.textport.Name = "textport";
            this.textport.Size = new System.Drawing.Size(81, 25);
            this.textport.TabIndex = 1;
            this.textport.Text = "5000";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(333, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 32);
            this.button1.TabIndex = 2;
            this.button1.Text = "开始监听";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboxSocket
            // 
            this.comboxSocket.FormattingEnabled = true;
            this.comboxSocket.Location = new System.Drawing.Point(458, 41);
            this.comboxSocket.Name = "comboxSocket";
            this.comboxSocket.Size = new System.Drawing.Size(192, 23);
            this.comboxSocket.TabIndex = 3;
            // 
            // textlog
            // 
            this.textlog.Location = new System.Drawing.Point(45, 82);
            this.textlog.Multiline = true;
            this.textlog.Name = "textlog";
            this.textlog.Size = new System.Drawing.Size(605, 119);
            this.textlog.TabIndex = 4;
            // 
            // textMessage
            // 
            this.textMessage.Location = new System.Drawing.Point(45, 218);
            this.textMessage.Multiline = true;
            this.textMessage.Name = "textMessage";
            this.textMessage.Size = new System.Drawing.Size(605, 119);
            this.textMessage.TabIndex = 5;
            // 
            // textFileName
            // 
            this.textFileName.Location = new System.Drawing.Point(45, 359);
            this.textFileName.Name = "textFileName";
            this.textFileName.Size = new System.Drawing.Size(407, 25);
            this.textFileName.TabIndex = 6;
            // 
            // btnChoose
            // 
            this.btnChoose.Location = new System.Drawing.Point(470, 353);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(75, 36);
            this.btnChoose.TabIndex = 7;
            this.btnChoose.Text = "选择";
            this.btnChoose.UseVisualStyleBackColor = true;
            // 
            // btnSendFile
            // 
            this.btnSendFile.Location = new System.Drawing.Point(551, 353);
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.Size = new System.Drawing.Size(99, 36);
            this.btnSendFile.TabIndex = 8;
            this.btnSendFile.Text = "发送文件";
            this.btnSendFile.UseVisualStyleBackColor = true;
            // 
            // btnSender
            // 
            this.btnSender.Location = new System.Drawing.Point(372, 401);
            this.btnSender.Name = "btnSender";
            this.btnSender.Size = new System.Drawing.Size(129, 37);
            this.btnSender.TabIndex = 9;
            this.btnSender.Text = "发送消息";
            this.btnSender.UseVisualStyleBackColor = true;
            this.btnSender.Click += new System.EventHandler(this.btnSender_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(518, 401);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(103, 37);
            this.button5.TabIndex = 10;
            this.button5.Text = "震动";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnSender);
            this.Controls.Add(this.btnSendFile);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.textFileName);
            this.Controls.Add(this.textMessage);
            this.Controls.Add(this.textlog);
            this.Controls.Add(this.comboxSocket);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textport);
            this.Controls.Add(this.textClient);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textClient;
        private System.Windows.Forms.TextBox textport;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboxSocket;
        private System.Windows.Forms.TextBox textlog;
        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.TextBox textFileName;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Button btnSendFile;
        private System.Windows.Forms.Button btnSender;
        private System.Windows.Forms.Button button5;
    }
}

