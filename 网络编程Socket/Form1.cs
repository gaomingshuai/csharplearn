﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 网络编程Socket
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//开始监听按钮
        {
            // 当点击监听的时候，在服务器端创建一个负责监听IP地址和端口号的Socket
            Socket socketWatch = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);//寻址方案ipv4 ipv6  , 套接字类型  ， 协议

            IPAddress ip = IPAddress.Any;//监听所有的ip

            //创建端口好对象
            IPEndPoint point = new IPEndPoint(ip, Convert.ToInt32(textport.Text));//将监听的ip和控件的文本组成端口对象

            //绑定监听端口
            socketWatch.Bind(point);
            ShowMsg("准备完毕，开始监听");

            socketWatch.Listen(10);//设置监听队列的长度为10，这个时间点内只有10个人，多了排队。

            //创建线程,持续接收请求
            Thread thread = new Thread(Listen);
            thread.IsBackground = true;
            thread.Start(socketWatch);

        }


        //将远程的客户端的IP地址和Socket存入集合中
        Dictionary<string, Socket> socketDictionary = new Dictionary<string, Socket>();
        /// <summary>
        /// 等待客户的连接，并且创建与之通信的Socket
        /// </summary>
        /// <param name="o"></param>
        /// 
        Thread threadRecive;
        Socket socketSender;
        void Listen(object o)
        {
            Socket socketWatch = o as Socket;
            while (true)
            {
                //等待客户端的连接,并且创建一个负责通信的Socket
                socketSender = socketWatch.Accept();

                socketDictionary.Add(socketSender.RemoteEndPoint.ToString(), socketSender);//将Socket 的ip和端口号 与Socket本身通过键值对的方式保存起来

                comboxSocket.Items.Add(socketSender.RemoteEndPoint.ToString());

                ShowMsg(socketSender.RemoteEndPoint.ToString() + "：连接成功");//192.168.112.78：连接成功

                //将该客户端的唯一标识发送回去
                string ip = socketSender.RemoteEndPoint.ToString();
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(ip);
                socketSender.Send(buffer);

                //开启一个新线程，不停的接收客户端信息
                threadRecive = new Thread(Recive);
                threadRecive.IsBackground = true;
                threadRecive.Start(socketSender);

            }
        }



        //服务器端不停的接收客户端发来的信息
        void Recive(object o)
        {
            Socket socket = o as Socket;
            while (true)
            {

                //客户端连接成功后，服务器应该接收客户端发来的消息
                byte[] buffer = new byte[1024 * 1024 * 10];//缓存默认10MB

                int res = socket.Receive(buffer);//实际的有效字节
                if (res == 0)
                {
                    break;
                }
                string str = Encoding.UTF8.GetString(buffer, 0, res);
                string[] strs = str.Split(',');

                Console.WriteLine(str);

                if (strs[strs.Length - 1].Equals("off"))////如果收到客户端发出off关闭请求，断开连接
                {
                   
                    socketDictionary[strs[0].ToString()].Dispose();
                    break;
                }
                else
                {
                    ShowMsg(socket.RemoteEndPoint + ":" + str);
                }
            }
        }

        void ShowMsg(string message)
        {
            textlog.AppendText(message + "\r\n");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;//停止检查
        }

        /// <summary>
        /// 服务器端给客户端发送信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSender_Click(object sender, EventArgs e)//发送信息按钮
        {
            string message = textMessage.Text.Trim();
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(message);
            if (comboxSocket.SelectedItem != null)
            {
                string ip = comboxSocket.SelectedItem.ToString();
                socketDictionary[ip].Send(buffer);
            }
            else
            {
                socketSender.Send(buffer);
            }



        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {



            foreach (var item in socketDictionary)
            {
                if (item.Value.Connected)
                {
                    
                    string message = "off";
                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(message);
                    item.Value.Send(buffer);
                    Thread.Sleep(socketDictionary.Count * 1000);
                    item.Value.Dispose();
                }


            }

            if (threadRecive != null)
                threadRecive.Abort();


        }
    }
}
