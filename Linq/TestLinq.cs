﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    internal class TestLinq
    {
        List<Student> studentList = new List<Student>() {

            new Student
            {
                Id = 1001,
                Name = "张三",
                Age = 20
            },new Student
            {
                Id = 1002,
                Name = "李四",
                Age = 22
            },new Student
            {
                Id = 1003,
                Name = "王五",
                Age = 25
            },new Student
            {
                Id = 1004,
                Name = "李雪",
                Age = 18
            },new Student
            {
                Id = 1005,
                Name = "刘琴",
                Age = 23
            },new Student
            {
                Id = 1006,
                Name = "张莉",
                Age = 20
            }

        };//数据源，用于查询使用

        public void Show()
        {
            {
                Console.WriteLine("-------------------------------普通查询-----------------------------------");
                foreach (var item in studentList)
                {
                    if (item.Age <= 22)
                    {
                        Console.WriteLine(item.ToString());
                    }
                }
            }

            {
                Console.WriteLine("-------------------------------linq查询（框架自带）-----------------------------------");
                var list = studentList.Where<Student>(s => s.Age <= 22);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
            }

            {
                Console.WriteLine("-------------------------------linq查询（自定义扩展方法）-----------------------------------");
                var list = studentList.ElementWhere(s => s.Age <= 22);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
            }

            {
                Console.WriteLine("-------------------------------linq查询（自定义扩展方法 泛型版本）-----------------------------------");
                var list = studentList.GenericElementWhere<Student>(s => s.Age <= 22);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
            }

                {
                    Console.WriteLine("-------------------------------关键字查询(linq)-----------------------------------");
                var list = from s in studentList
                           where s.Age <= 22
                           select s;
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
            }




        }

    }

    /// <summary>
    /// 扩展方法类
    /// </summary>
    public static class ExtendMethod
    {
        /// <summary>
        /// 针对Student类的查询扩展方法
        /// </summary>
        /// <param name="resources"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        internal static List<Student> ElementWhere(this List<Student> resources,Func<Student,bool> func)
        {
            List<Student> list = new List<Student>();
            foreach (var item in resources)
            {
                if (func.Invoke(item))
                {
                    list.Add(item);
                }
            }
            return list;
        }

        /// <summary>
        /// 泛型扩展方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resources"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        public static List<T> GenericElementWhere<T>(this List<T> resources, Func<T, bool> func)
        {
            List<T> list = new List<T>();
            foreach (var item in resources)
            {
                if (func.Invoke(item))
                {
                    list.Add(item);
                }
            }
            return list;
        }
    }
}
