﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    internal class InternalClassAndVar
    {
        /// <summary>
        /// 内部类撰写，类中的内容只读，成员无需写类型，但要初始化，编译器根据初始化内容，确定类型
        /// </summary>
        public void ShowInternalClass()
        {

            object model = new
            {
                a = 1,
                b = "开心1"
            };
            //Console.WriteLine(model.a);//报错，object类型，编译器不允许


            dynamic dyModel = new
            {
                a = 2,
                b = "开心2"
            };
            Console.WriteLine(dyModel.a);
            Console.WriteLine(dyModel.b);
            //dyModel.a = 5;//dynamic 只读，不可写，但是编译可通过，运行报错


            var varModel = new
            {
                a = 3,
                b = "开心3"
            };
            Console.WriteLine(varModel.a);
            Console.WriteLine(varModel.b);
            //varModel.a = 10;//报错，var，只读，不可更改

        }


        /// <summary>
        ///  var是语法糖，有自动推测的功能，可根据实例确定类型
        ///     配合匿名类使用
        ///     偷懒
        /// </summary>
        public void ShowVar()
        {
            var a = 1;//int
            var b = "string";//string
            var c = 'a';//char
            var d = 1.38;//double
            Console.WriteLine(a.GetType()+","+a);
            Console.WriteLine(b.GetType()+","+b);
            Console.WriteLine(c.GetType()+","+c);
            Console.WriteLine(d.GetType()+","+d);

            //var aa;//报错，没有实例，无法推测数据类型
            //a = "adien";//报错，数据类型确定后，不能再次修改数据类型
        }
    }
}
