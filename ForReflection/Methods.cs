﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForReflection
{
    public class Methods
    {

        public void Test1()
        {
            Console.WriteLine("Test1方法，没有参数");
        }
        public void Test2(int id)
        {
            Console.WriteLine("Test2方法，有一个参数，参数为："+id);
        }
        public void Test3(int id ,string name)
        {
            Console.WriteLine("Test3方法，有两个参数，int类型参数为：" + id+"string类型参数为："+name);
        }
        public void Test3(int id)
        {
            Console.WriteLine("Test3方法，有一个int类型参数，参数为：" + id);
        }
        public void Test3(string name)
        {
            Console.WriteLine("Test3方法，有一个string类型参数，参数为：" + name);
        }
        
        public void Test3()
        {
            Console.WriteLine("Test3方法，没有参数");
        }
        
        private void Test4()
        {
            Console.WriteLine("Test4方法，私有方法,没有参数");
        } 
        public static void Test5(string name)
        {
            Console.WriteLine("Test5方法，有一个string类型参数，参数为：" + name);
        }


    }
}
