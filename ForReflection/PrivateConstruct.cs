﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForReflection
{
    public class PrivateConstruct : IDBHelper
    {
        private PrivateConstruct()
        {
            Console.WriteLine(this.GetType+"的私有无参构造");
        }

        public void Query()
        {
            Console.WriteLine(this.GetType()+"的构造方法是私有的！");
        }
    }
}
