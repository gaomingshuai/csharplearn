﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForReflection
{
    public class GenericClass<T, K>
    {
        public GenericClass()
        {
            Console.WriteLine("我是泛型类的构造类，"+this.GetType()+"已经创建好了");
        }

        public void Query()
        {
            Console.WriteLine("我是泛型类中的普通方法。");
        }

        public void Test<X,Y>(X x,Y y)//泛型类中的泛型方法
        {
            Console.WriteLine("第一个参数类型为：{0}，第二个参数类型为：{1}",x.GetType().Name,y.GetType().Name);
        }
    }

    public class GenericMethod
    {
        public void Test<T,K>(T t,K k)
        {
            Console.WriteLine("第一个参数类型为：{0}，第二个参数类型为：{1}", t.GetType().Name, k.GetType().Name);
        }
    }
}
