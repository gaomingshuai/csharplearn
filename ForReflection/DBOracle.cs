﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForReflection
{
    public class DBOracle
    {
        public DBOracle()
        {
            Console.WriteLine("{0}被构造出来了", this.GetType().Name);
        }

        public void Query()
        {
            Console.WriteLine("{0}.Query", this.GetType().Name);
        }
    }
}
