﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForReflection
{
    public class DBSqlHaveParametrrs : IDBHelper
    {
        public DBSqlHaveParametrrs()
        {
            Console.WriteLine(this.GetType()+"通过无参构造被构造出来了");
        } 
        public DBSqlHaveParametrrs(string name)
        {
            Console.WriteLine(this.GetType() + "通过传入类型为 string 参数的构造函数被构造出来了，参数为：" +name);

        }
        public DBSqlHaveParametrrs(int age)
        {
            Console.WriteLine(this.GetType() + "通过传入类型为 int 参数的构造函数被构造出来了，参数为：" + age);
        }
        public DBSqlHaveParametrrs(string name,int age)
        {
            Console.WriteLine(this.GetType() + "通过传入类型为 string 和 int 参数的构造函数被构造出来了，参数1为：" + name+",参数2为：" + age);
        }

        public void Query()
        {
            Console.WriteLine(this.GetType() + "用来测试不同参数构造创建对象");
        }
    }
}
