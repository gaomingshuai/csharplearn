﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.demo
{
    internal class 图片复制demo
    {
        public void CopyJpgDemo()
        {

            string pathRead = @"D:\System\Desktop\刘亦菲.jpg";
            string pathWrite = @"../../../img/刘亦菲1.jpg";

            using (FileStream fsRead = new FileStream(pathRead, FileMode.OpenOrCreate, FileAccess.Read))
            {
                using (FileStream fsWrite = new FileStream(pathWrite, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    byte[] bufferRead = new byte[1024 * 1024 * 5];//默认是5MB


                    while (true)//循环读取,写入大文件
                    {
                        int res = fsRead.Read(bufferRead, 0, bufferRead.Length);

                        fsWrite.Write(bufferRead, 0, res);//写入真实数据

                        if (res == 0) break;
                    }


                }
            }
        }
    }
}
