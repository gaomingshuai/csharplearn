﻿using System.Text;

internal class StreamRead
{
    private string path;
    private Encoding @default;

    public StreamRead(string path)
    {
        this.path = path;
    }

    public StreamRead(string path, Encoding @default)
    {
        this.path = path;
        this.@default = @default;
    }
}