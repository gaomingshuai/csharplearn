﻿/*#define PI*/
#undef PI
/*#define PI2*/
#undef PI2
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 预处理
{
    internal class Class1
    {
        public void PIs()
        {
#if (PI)
            Console.WriteLine("PI is defined");
#elif(PI2)
             Console.WriteLine("PI is defined and PI2 is not defined");
#else
            global::System.Console.WriteLine("PI and PI2 is both not defined");
#endif
        }
    }
}
