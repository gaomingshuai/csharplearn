﻿using System.Data;
using System.Data.SqlClient;

/*ConnectToSqlServer();//查看是否建立连接*/
/*QueryAll();//查询所有用户信息*/
/*QueryUserByUserId();//根据用户Id获取用户信息*/
/*InsertUser();//插入用户信息*/
/*UpdateUserByUserId();//根据用户Id修改用户信息*/

/*DeleteUserByUserId();//根据用户Id删除用户信息*/



#region C#与SqlServer连接演示

void ConnectToSqlServer()
{
    //1、新建数据库连接对象
    SqlConnection connection = new SqlConnection();

    //2、给连接对象指定连接参数
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";

    //3、开始建立连接
    connection.Open();

    //4、输出提示
    if (connection.State == System.Data.ConnectionState.Open)
    {
        Console.WriteLine("已经建立连接了！");
    }
    else
    {
        Console.WriteLine("数据库连接失败!");
    }
}
#endregion 

#region 查询所有信息
void QueryAll()
{
    //1、新建连接
    SqlConnection connection = new SqlConnection();

    //2、指定连接所需信息：连接名，数据库名，用户名，密码；
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";

    //3、书写sql语句
    string sql = "select * from dbo.user_Test";

    try
    {
        //4、创建执行对象
        SqlCommand command = new SqlCommand(sql, connection);

        //5、建立连接
        connection.Open();

        //6、执行并返回结果
        SqlDataReader reader = command.ExecuteReader();

        //7、操作结果，其中括号内的数字代表着数据库中的列
        while (reader.Read())
        {
            Console.Write(reader.GetInt64(0) + " ");
            Console.Write(reader.GetString(1) + " ");
            Console.Write(reader.GetString(2) + " ");
            Console.WriteLine(reader.GetInt32(3));
        }
        //8、关闭连接
        reader.Close();
        connection.Close();

    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        
    }
}
#endregion

#region 根据user_Id获取用户信息
void QueryUserByUserId()
{
    //1、创建连接对象
    SqlConnection connection = new SqlConnection();

    //2、指定连接所需要的信息: 连接名 数据库名 用户名 密码
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";

    //3、创建可执行参数
    string sql = "select * from user_Test where user_Id = @user_Id";
    SqlCommand command = new SqlCommand(sql, connection);

    //4、提供sql参数
    command.Parameters.AddWithValue("user_Id", 1001);

    //5、建立连接
    connection.Open();

    //6、执行sql语句，获取返回结果集
    SqlDataReader reader = command.ExecuteReader();

    //7、操纵结果集
    while (reader.Read())
    {
        Console.Write(reader.GetInt64(0)+" ");
        Console.Write(reader.GetString(1)+" ");
        Console.Write(reader.GetString(2)+" ");
        Console.WriteLine(reader.GetInt32(3));
    }

    //8、关闭连接
    reader.Close();
    connection.Close();
}
#endregion

#region 插入信息
void InsertUser()
{
    //1、获取连接对象
    SqlConnection connection = new SqlConnection();

    //2、指定连接所需信息:连接名 数据库名 用户名 密码
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";

    //3、准备sql语句所需信息
    int user_Id = 1007;
    string user_Name = "小利";
    char sex = '男';
    int age = 22;

    //4、书写sql语句
    string sql = $"insert into user_Test(user_Id,user_name,sex,age) values({user_Id},@user_Name,@sex,{age})";

    //5、创建可执行文件
    SqlCommand command = new SqlCommand(sql, connection);
    command.Parameters.AddWithValue("user_Name", user_Name);
    command.Parameters.AddWithValue("sex", sex);

    //6、建立连接
    connection.Open();

    //7、执行sql语句
    int res = command.ExecuteNonQuery();

    //8、操作结果集
    if (res != -1)
    {
        QueryAll();
    }
    else
    {
        Console.WriteLine("插入数据失败！！");
    }

    //9、关闭连接
    connection.Close();

}

#endregion

#region 根据用户Id修改用户信息
void UpdateUserByUserId()
{
    SqlConnection connection = new SqlConnection();
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";


    int id = 1006;
    string userName = "小利";
    string sql = $"update user_Test set user_name = @userName where user_Id = {id}";

    SqlCommand command = new SqlCommand(sql, connection);
    command.Parameters.AddWithValue("userName", userName);

    connection.Open();

    int res = command.ExecuteNonQuery();

    if(res != -1)
    {
        QueryAll();
    }
    else
    {
        Console.WriteLine("修改失败！");
    }

    connection.Close();
}
#endregion


#region 根据用户id删除用户信息
void DeleteUserByUserId()
{
    SqlConnection connection = new SqlConnection();
    connection.ConnectionString = "Data Source=DESKTOP-I8R7K6J;Initial Catalog=my_Test01;User Id=sa;Password=123456;";

    int id = 1007;
    string sql = $"delete from user_Test where user_Id = {id}";

    SqlCommand command = new SqlCommand(sql, connection);

    connection.Open();

    int res = command.ExecuteNonQuery();

    if (res != -1)
    {
        QueryAll();
    }
    else
    {
        Console.WriteLine("删除失败");
    }
    connection.Close();
}
#endregion